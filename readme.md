# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* BaseControllerIT for integration tests

* BaseControllerTest for Spring Unit Tests


### Endpoints
The following guides illustrate how to use some features concretely:

* Get: url='/', reponse Greeting As Json

* Post: url='/highest'  , requenst = SentenceInfo, reponse ResponseWordFreq As Json

* Post: url='/wordInfo' , requenst = SentenceInfo, reponse ResponseWordFreq As Json

* Post: url='/wordsInfo', requenst = SentenceInfo, reponse ResponseMultipleWordsFreq As Json

