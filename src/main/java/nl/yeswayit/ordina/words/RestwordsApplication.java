package nl.yeswayit.ordina.words;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestwordsApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestwordsApplication.class, args);
    }

}
