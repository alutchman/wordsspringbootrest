package nl.yeswayit.ordina.words.controllers;

import nl.yeswayit.ordina.words.requests.SentenceInfo;
import nl.yeswayit.ordina.words.response.Greeting;
import nl.yeswayit.ordina.words.response.ResponseMultipleWordsFreq;
import nl.yeswayit.ordina.words.response.ResponseWordFreq;
import nl.yeswayit.ordina.words.utils.WordFrequencyAnalyzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.atomic.AtomicLong;

@RestController
public class BaseController {
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @Autowired
    private WordFrequencyAnalyzer wordFrequencyAnalyzer;

    @RequestMapping("/")
    public Greeting index(@RequestParam(value = "name", defaultValue = "World") String name) {
        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }

    @PostMapping("/highest")
    public ResponseWordFreq analyzeWords(@RequestBody SentenceInfo input) {
        return wordFrequencyAnalyzer.calculateHighestFrequency(input);
    }

    @PostMapping("/wordInfo")
    public ResponseWordFreq wordInfo(@RequestBody SentenceInfo input) {
        return wordFrequencyAnalyzer.calculateFrequencyForWord(input);
    }

    @PostMapping("/wordsInfo")
    public ResponseMultipleWordsFreq wordsInfo(@RequestBody SentenceInfo input) {
        return wordFrequencyAnalyzer.calculateMostFrequentNWords(input);
    }
}
