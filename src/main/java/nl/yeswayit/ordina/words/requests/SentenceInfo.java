package nl.yeswayit.ordina.words.requests;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SentenceInfo {
    private String sentence;
    private String argValue;

    public SentenceInfo(String sentence) {
        this.sentence = sentence;
    }
}
