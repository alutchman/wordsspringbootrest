package nl.yeswayit.ordina.words.response;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class Greeting {
    private final long id;
    private final String content;
}
