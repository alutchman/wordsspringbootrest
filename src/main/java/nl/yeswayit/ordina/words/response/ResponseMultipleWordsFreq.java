package nl.yeswayit.ordina.words.response;

import lombok.*;

import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class ResponseMultipleWordsFreq {
    private String data;
    private int number;
    private List<ResponseWordOfManyFreq> values ;

    public List<ResponseWordOfManyFreq> getValues() {
        if (values == null) {
            values = new ArrayList<>();
        }
        return values;
    }
}
