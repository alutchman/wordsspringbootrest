package nl.yeswayit.ordina.words.response;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class ResponseWordFreq {
    private String data;
    private int frequency;
    private String word;
}
