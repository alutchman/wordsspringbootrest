package nl.yeswayit.ordina.words.response;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class ResponseWordOfManyFreq {
    private int frequency;
    private String word;
}
