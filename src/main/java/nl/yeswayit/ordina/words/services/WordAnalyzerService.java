package nl.yeswayit.ordina.words.services;


import nl.yeswayit.ordina.words.requests.SentenceInfo;
import nl.yeswayit.ordina.words.response.ResponseMultipleWordsFreq;
import nl.yeswayit.ordina.words.response.ResponseWordFreq;
import nl.yeswayit.ordina.words.response.ResponseWordOfManyFreq;
import nl.yeswayit.ordina.words.utils.WordFrequency;
import nl.yeswayit.ordina.words.utils.WordFrequencyAnalyzer;
import nl.yeswayit.ordina.words.utils.WordFrequencyInfo;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Service
public class WordAnalyzerService implements WordFrequencyAnalyzer {

    public ResponseWordFreq calculateHighestFrequency(SentenceInfo request) {
        ResponseWordFreq response = new ResponseWordFreq();
        response.setData(request.getSentence());
        if (request.getSentence() == null || request.getSentence().trim().length() ==0 ) {
            response.setFrequency(0);
            return response;
        }
        List<WordFrequency> wordlist = orginaizeText(request.getSentence());

        WordFrequency freqInfo = wordlist.get(0);
        response.setData(request.getSentence());
        response.setWord(freqInfo.getWord());
        response.setFrequency(freqInfo.getFrequency());
        return response;
    }

    public ResponseWordFreq calculateFrequencyForWord(SentenceInfo request) {
        WordFrequencyInfo freqInfo = new WordFrequencyInfo(request.getArgValue());
        Arrays.asList(request.getSentence().split("\\s")).forEach(input -> {
            if (input.equalsIgnoreCase(request.getArgValue())) {
                freqInfo.incFrequency();
            }
        });
        ResponseWordFreq response = new ResponseWordFreq();
        response.setData(request.getSentence());
        response.setWord(freqInfo.getWord());
        response.setFrequency(freqInfo.getFrequency());
        return response;
    }


    public ResponseMultipleWordsFreq calculateMostFrequentNWords(SentenceInfo request) {
        List<WordFrequency> wordlist = orginaizeText(request.getSentence());
        int maxNum = wordlist.size();
        List<WordFrequency> result;

        int numbertoCheck = Integer.parseInt(request.getArgValue());

        if (numbertoCheck < maxNum) {
            result =  wordlist.subList(0, numbertoCheck);
        } else {
            result = wordlist;
        }

        List<ResponseWordOfManyFreq> values = new ArrayList<>();
        Consumer<WordFrequency> transformForResonse = item -> {
            ResponseWordOfManyFreq transformed = new ResponseWordOfManyFreq();
            transformed.setWord(item.getWord());
            transformed.setFrequency(item.getFrequency());
            values.add(transformed);
        };
        result.forEach(transformForResonse);

        ResponseMultipleWordsFreq response = new ResponseMultipleWordsFreq();
        response.setData(request.getSentence());
        response.setNumber(numbertoCheck);
        response.setValues(values);
        return response;
    }


    private List<WordFrequency> orginaizeText(String text) {
        List<String> wordlist = fromSentence(text);
        final Map<String, WordFrequencyInfo> mapped = new HashMap<String, WordFrequencyInfo>();

        Consumer<String> organizer = input -> {
            String toMap = input.toLowerCase();
            if (mapped.containsKey(toMap)) {
                WordFrequencyInfo existingItem = mapped.get(toMap);
                existingItem.incFrequency();
            } else {
                WordFrequencyInfo newItem = new WordFrequencyInfo(toMap);
                newItem.incFrequency();
                mapped.put(toMap, newItem);
            }
        };
        wordlist.forEach(organizer);
        List<WordFrequency> preList = mapped.values().stream().collect(Collectors.toList());

        preList.sort((u, v) -> {
            if (u.getFrequency() == v.getFrequency()) {
                return String.CASE_INSENSITIVE_ORDER.compare(u.getWord(), v.getWord());
            }
            return u.getFrequency() > v.getFrequency() ? -1 : 1;
        });
        return preList;
    }

    private List<String> fromSentence(String input) {
        if (input == null) {
            return new ArrayList<>();
        }
        input = input.replace(".", " ").replace(",", " ").
                replace("?", " ").replace("!", " ");
        return Arrays.asList(input.split("\\s")).stream().map(String::trim).
                filter( item -> item.length() > 0).collect(Collectors.toList());
    }


}
