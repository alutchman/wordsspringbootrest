package nl.yeswayit.ordina.words.utils;

public interface WordFrequency {
    String getWord();
    int getFrequency();
}