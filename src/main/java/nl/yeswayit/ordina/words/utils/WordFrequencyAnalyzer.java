package nl.yeswayit.ordina.words.utils;

import nl.yeswayit.ordina.words.requests.SentenceInfo;
import nl.yeswayit.ordina.words.response.ResponseMultipleWordsFreq;
import nl.yeswayit.ordina.words.response.ResponseWordFreq;

public interface WordFrequencyAnalyzer {
    ResponseWordFreq calculateHighestFrequency(SentenceInfo request);
    ResponseWordFreq calculateFrequencyForWord(SentenceInfo request);
    ResponseMultipleWordsFreq calculateMostFrequentNWords(SentenceInfo request);
}
