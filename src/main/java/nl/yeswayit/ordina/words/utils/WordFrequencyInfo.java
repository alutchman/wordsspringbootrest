package nl.yeswayit.ordina.words.utils;

public class WordFrequencyInfo implements WordFrequency {
    private String word;
    private int frequency;

    public WordFrequencyInfo(String word) {
        this.word = word;
        this.frequency = 0;
    }

    public String getWord() {
        return word;
    }

    public int getFrequency() {
        return frequency;
    }


    public void incFrequency(){
        frequency++;

    }

    @Override
    public String toString() {
        return String.format("(\"%s\", %d)", word, frequency);
    }
}
