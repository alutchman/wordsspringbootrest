package nl.yeswayit.ordina.words.controllers;

import nl.yeswayit.ordina.words.requests.SentenceInfo;
import nl.yeswayit.ordina.words.response.Greeting;
import nl.yeswayit.ordina.words.response.ResponseMultipleWordsFreq;
import nl.yeswayit.ordina.words.response.ResponseWordFreq;
import nl.yeswayit.ordina.words.response.ResponseWordOfManyFreq;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

import java.net.URL;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BaseControllerIT {
    @LocalServerPort
    private int port;

    private URL base;

    @Autowired
    private TestRestTemplate template;

    @BeforeEach
    public void setUp() throws Exception {
        this.base = new URL("http://localhost:" + port + "/");
    }

    @Test
    public void getHello() throws Exception {
        Greeting expected = new Greeting(1, "Hello, Ordina!");
        ResponseEntity<Greeting> response = template.getForEntity(base.toString()+"?name=Ordina",
                Greeting.class);
        assertThat(response.getBody()).isEqualTo(expected);
    }

    @Test
    public void getHighest() throws Exception {
        SentenceInfo request = new SentenceInfo("Dit is een zin. En dit is nog een langere zin. Zin in ijsjes ?");
        ResponseWordFreq expected = new ResponseWordFreq(request.getSentence(), 3, "zin");
        ResponseEntity<ResponseWordFreq> response = template.postForEntity(base.toString()+"highest", request, ResponseWordFreq.class);
        assertThat(response.getBody()).isEqualTo(expected);
    }

    @Test
    public void getWordFrequency() throws Exception {
        SentenceInfo request = new SentenceInfo("Dit is een zin. En dit is nog een langere zin. Zin in ijsjes ?","een");
        ResponseWordFreq expected = new ResponseWordFreq(request.getSentence(), 2, "een");
        ResponseEntity<ResponseWordFreq> response = template.postForEntity(base.toString()+"wordInfo", request, ResponseWordFreq.class);
        assertThat(response.getBody()).isEqualTo(expected);
    }

    @Test
    public void getWordsFrequency() throws Exception {
        String test = "The sun shines over the lake.";
        SentenceInfo request = new SentenceInfo(test,"3");

        ResponseMultipleWordsFreq expected = new ResponseMultipleWordsFreq();
        expected.setData(test);
        expected.setNumber(Integer.parseInt(request.getArgValue()));
        expected.getValues().add(new ResponseWordOfManyFreq(2, "the"));
        expected.getValues().add(new ResponseWordOfManyFreq(1, "lake"));
        expected.getValues().add(new ResponseWordOfManyFreq(1, "over"));

        ResponseEntity<ResponseMultipleWordsFreq> response = template.postForEntity(base.toString()+"wordsInfo",
                request, ResponseMultipleWordsFreq.class);
        assertThat(response.getBody()).isEqualTo(expected);
    }


    @Test
    public void getWordsFrequencyEmpty() throws Exception {
        String test = "";
        SentenceInfo request = new SentenceInfo(test,"2");

        ResponseMultipleWordsFreq expected = new ResponseMultipleWordsFreq();
        expected.setData(test);
        expected.setNumber(Integer.parseInt(request.getArgValue()));

        ResponseEntity<ResponseMultipleWordsFreq> response = template.postForEntity(base.toString()+"wordsInfo",
                request, ResponseMultipleWordsFreq.class);
        assertThat(response.getBody()).isEqualTo(expected);
    }

    @Test
    public void getWordsFrequencyNull() throws Exception {
        String test = null;
        SentenceInfo request = new SentenceInfo(test,"2");

        ResponseMultipleWordsFreq expected = new ResponseMultipleWordsFreq();
        expected.setData(test);
        expected.setNumber(Integer.parseInt(request.getArgValue()));

        ResponseEntity<ResponseMultipleWordsFreq> response = template.postForEntity(base.toString()+"wordsInfo",
                request, ResponseMultipleWordsFreq.class);

        assertThat(response.getBody()).isEqualTo(expected);
    }
}
