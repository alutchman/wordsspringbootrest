package nl.yeswayit.ordina.words.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.yeswayit.ordina.words.requests.SentenceInfo;
import nl.yeswayit.ordina.words.response.ResponseMultipleWordsFreq;
import nl.yeswayit.ordina.words.response.ResponseWordFreq;
import nl.yeswayit.ordina.words.response.ResponseWordOfManyFreq;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class BaseControllerTest {
    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void getHello() throws Exception {
         mvc.perform(MockMvcRequestBuilders.get("/").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("{\"id\":1,\"content\":\"Hello, World!\"}")));
    }

    @Test
    public void getHighest() throws Exception {
        SentenceInfo request = new SentenceInfo("Dit is een zin. En dit is nog een langere zin. Zin in ijsjes ?");
        ResponseWordFreq expected = new ResponseWordFreq(request.getSentence(), 3, "zin");

        String json = mapper.writeValueAsString(request);
        String responseExpect = mapper.writeValueAsString(expected);

        mvc.perform(MockMvcRequestBuilders.post("/highest")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo(responseExpect)));

    }

    @Test
    public void getWordFrequency() throws Exception {
        SentenceInfo request = new SentenceInfo("Dit is een zin. En dit is nog een langere zin. Zin in ijsjes ?","een");
        ResponseWordFreq expected = new ResponseWordFreq(request.getSentence(), 2, "een");


        String json = mapper.writeValueAsString(request);
        String responseExpect = mapper.writeValueAsString(expected);

        mvc.perform(MockMvcRequestBuilders.post("/wordInfo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo(responseExpect)));
    }

    @Test
    public void getWordsFrequency() throws Exception {
        String test = "The sun shines over the lake.";
        SentenceInfo request = new SentenceInfo(test,"3");

        ResponseMultipleWordsFreq expected = new ResponseMultipleWordsFreq();
        expected.setData(test);
        expected.setNumber(Integer.parseInt(request.getArgValue()));
        expected.getValues().add(new ResponseWordOfManyFreq(2, "the"));
        expected.getValues().add(new ResponseWordOfManyFreq(1, "lake"));
        expected.getValues().add(new ResponseWordOfManyFreq(1, "over"));

        String json = mapper.writeValueAsString(request);
        String responseExpect = mapper.writeValueAsString(expected);

        mvc.perform(MockMvcRequestBuilders.post("/wordsInfo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo(responseExpect)));
    }


    @Test
    public void getWordsFrequencyEmpty() throws Exception {
        String test = "";
        SentenceInfo request = new SentenceInfo(test,"2");

        ResponseMultipleWordsFreq expected = new ResponseMultipleWordsFreq();
        expected.setData(test);
        expected.setNumber(Integer.parseInt(request.getArgValue()));

        String json = mapper.writeValueAsString(request);
        String responseExpect = mapper.writeValueAsString(expected);

        mvc.perform(MockMvcRequestBuilders.post("/wordsInfo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo(responseExpect)));
    }

    @Test
    public void getWordsFrequencyNull() throws Exception {
        String test = null;
        SentenceInfo request = new SentenceInfo(test,"2");

        ResponseMultipleWordsFreq expected = new ResponseMultipleWordsFreq();
        expected.setData(test);
        expected.setNumber(Integer.parseInt(request.getArgValue()));

        String json = mapper.writeValueAsString(request);
        String responseExpect = mapper.writeValueAsString(expected);

        mvc.perform(MockMvcRequestBuilders.post("/wordsInfo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo(responseExpect)));
    }
}